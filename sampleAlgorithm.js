//チームID 0:なし, 1:赤, 2:青

/**
 * @typedef {Object} Tile タイルの情報
 * @prop {Number} point 得点
 * @prop {Number} owner 所有チームID
 */

/**
 * @typedef {Object} Agent エージェントの情報
 * @prop {Number} id エージェントID
 * @prop {Number} teamId チームID
 * @prop {Number} x x座標
 * @prop {Number} y y座標
 */

 /**
  * @typedef {Object} Action エージェントの行動を指定する
  * @prop {Number} agentId エージェントID
  * @prop {Number} dx x軸の移動量 -1<dx<1
  * @prop {Number} dy y軸の移動量 -1<dy<1
  */

/**
 * 自動化にチェックいれた状態で決定を押したときに呼び出される
 * @param {Tile[][]} tiles Tileの2次元配列
 * @param {Agent[]} agents Agentの配列
 * @param {Action[]} actions Actionの配列
 * @param {Object} Var 自由に使用できる。値は保持される。
 */

ENC.redAlgorithm = function(tiles, agents, actions, Var){

  //エージェントIDから対応するアクションを探す。見つからなければundefined
  let findActionByAgentID = function(agentId){ 
    return actions.find(one=>one.agentId == agentId)
  }

  //printf のような関数
  console.log('in redAlgorithm'); //文字列
  console.log(actions); //変数(オブジェクト)
  console.log(`最初のエージェントのID:${agents[0].id}`); //文字列+変数

  //Varの使用例
  if(!("x" in Var)) //("x" in Var) で Var.xが存在するなら
    Var.x = 0;       //"x"がVarに存在しなければ Var.x=0 (初期化)
  else
    Var.x++;        //"x"がVarに存在すればVar.xの値を１増やす。(2回目以降)

  //actionsの使用例
  for(let i=0; i<actions.length; i++)
    actions[i].dx = -1;     //すべてのactionのdxを-1に (すべてのエージェントが左へ進む)

  //赤チームのエージェントごとの処理の例
  for(let i=0; i<agents.length; i++){ 
    let agent = agents[i];
    if(agent.teamId != 1) //赤チームでなければ
      continue;
    console.log(`${i}番目のエージェントのID:${agent.id}`)
    console.log(`${i}番目のエージェントのx座標:${agent.x}`)
    console.log(`${i}番目のエージェントのy座標:${agent.y}`)
    console.log(`${i}番目のエージェントがいるタイルの得点:${tiles[agent.y][agent.x].point}`)
    let action = findActionByAgentID(agent.id);
    action.dx = 0;
    action.dy = 1; //上にすすむ
  }
  console.log(actions);
}