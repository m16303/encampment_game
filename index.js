var ENC={}
ENC.params={}
ENC.jsons=[]
ENC.redAlgorithm=null;
ENC.blueAlgorithm=null;
ENC.redVariable = {}
ENC.blueVariable = {}
function $(selector){
  return document.querySelector(selector);
}

ENC.getRequest = function(url){
  return new Promise((resolve,reject)=>{
    let xhr = new XMLHttpRequest();
    xhr.open('GET',url,true);
    xhr.onload = function () {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          resolve(xhr);
        }
        else{
          reject(xhr);
        }
      }
    };
    xhr.send();
  })
}

ENC.matchDiv = $("#matchDiv");
ENC.fieldDiv = $("#fieldDiv");
ENC.toolDiv = $("#toolDiv");
ENC.infoDiv  = $("#infoDiv");
ENC.confirmButton = $("#confirmButton");
ENC.redAutoRadio = $("#redAutoRadio");
ENC.blueAutoRadio = $("#blueAutoRadio");