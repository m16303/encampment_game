//チームID 0:なし, 1:赤, 2:青

/**
 * @typedef {Object} Tile タイルの情報
 * @prop {Number} point 得点
 * @prop {Number} owner 所有チームID
 */

/**
 * @typedef {Object} Agent エージェントの情報
 * @prop {Number} id エージェントID
 * @prop {Number} teamId チームID
 * @prop {Number} x x座標
 * @prop {Number} y y座標
 */

 /**
  * @typedef {Object} Action エージェントの行動を指定する
  * @prop {Number} agentId エージェントID
  * @prop {Number} dx x軸の移動量 -1<dx<1
  * @prop {Number} dy y軸の移動量 -1<dy<1
  */

/**
 * 自動化にチェックいれた状態で決定を押したときに呼び出される
 * @param {Tile[][]} tiles Tileの2次元配列
 * @param {Agent[]} agents Agentの配列
 * @param {Action[]} actions Actionの配列
 * @param {Object} Var 自由に使用できる。値は保持される。
 */
ENC.blueAlgorithm = function(tiles, agents, actions, Var){
}