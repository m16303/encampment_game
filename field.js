ENC.TEAMCOLOR={0:null,1:"red",2:"blue"};
ENC.params.tileWidth=30;
ENC.params.tileHeight=30;
ENC.params.agentWidth=16;
ENC.params.agentHeight=16;

ENC.data={};
ENC.UI={};
ENC.UI.turnLimit=30;
ENC.UI.firstTeam=1; //1:red, 2:blue

ENC.newTileElement = function(point,color,x,y){
  let tile = document.createElement("div");
  tile.classList.add("tile");
  if(color === "red")
    tile.classList.add("redtile");
  else if(color === "blue")
    tile.classList.add("bluetile");
  else
    tile.classList.add("whitetile");
  tile.style.width=ENC.params.tileWidth+"px";
  tile.style.height=ENC.params.tileHeight+"px";
  tile.innerHTML = String(point);
  let marginTop = Number(fieldDiv.style.marginTop.replace(/px/,""));
  let marginLeft = Number(fieldDiv.style.marginLeft.replace(/px/,""));
  tile.style.left = marginLeft+(ENC.params.tileWidth*x)+"px";
  tile.style.top = marginTop+(ENC.params.tileHeight*y)+"px";
  return tile;
}

ENC.newAgent = function(id){
  let agent = document.createElement("div");
  agent.classList.add("agent");
  agent.innerHTML=id;
  agent.style.width = ENC.params.agentWidth+"px";
  agent.style.height = ENC.params.agentHeight+"px";
  agent.style.borderRadius = ENC.params.agentWidth+"px";
  agent.style.fontSize = (ENC.params.agentWidth-2) +"px";
  return agent;
}

ENC.calcPoints=function(tiles){
  let total = {};
  total.tilePoints=[0,0];
  total.areaPoints=[0,0];
  let width = tiles[0].length;
  let height = tiles.length;

  let checked = [Array(tiles.length).fill(Array(tiles[0].length).fill(false)),Array(tiles.length).fill(Array(tiles[0].length).fill(false))];

  for(let y=0; y<height; y++){ //tilePoint
    let row = tiles[y]
    for(let x=0; x<width; x++){
      if(row[x].owner == 1)
        total.tilePoints[0] += row[x].point;
      else if(row[x].owner == 2)
        total.tilePoints[1] += row[x].point;
    }
  }

  let spread = function(checked,x,y,teamId){
    if(checked[y][x]===false){
      checked[y][x] = true;
      if(tiles[y][x].owner!=teamId){
        if(x-1>0) 
          spread(checked,x-1,y,teamId);
        if(x+1<width)
          spread(checked,x+1,y,teamId);
        if(y-1>0)
          spread(checked,x,y-1,teamId);
        if(y+1<height)
          spread(checked,x,y+1,teamId);
      }
    }
  }

  for(let y of [0,height-1]){ //Top,Bottom
    for(let x=0; x<width; x++){
      spread(checked[0],x,y,1); //red
      spread(checked[1],x,y,2); //blue
    }
  }
  for(let x of [0,width-1]){ //Left,Right
    for(let y=0; y<height; y++){
      spread(checked[0],x,y,1); //red
      spread(checked[1],x,y,2); //blue
    }
  }

  for(let y=0; y<height; y++){ //areaPoint
    for(let x=0; x<width; x++){
      if(checked[0][y][x]===false && tiles[y][x].owner!=1) //red
        total.areaPoints[0] += Math.abs(tiles[y][x].point)
      if(checked[1][y][x]===false && tiles[y][x].owner!=2) //red
        total.areaPoints[1] += Math.abs(tiles[y][x].point)
    }
  }

  return total;
}

ENC.Tile = class{
  constructor(point, owner, x, y){
    this.point = point;
    this.owner = owner;
    this.agentId = null;
    this.x = x;
    this.y = y;
    this.updateElement();
  }

  updateElement(){
    this.element = ENC.newTileElement(this.point,ENC.TEAMCOLOR[this.owner],this.x,this.y);
    this.element.addEventListener("click",()=>{
      this.element.disabled = true;
      ENC.clickTile(this);
      this.element.disabled = false;
    })
  }
}

ENC.Agent = class{
  constructor(id, teamId, x, y){
    this.id = id;
    this.teamId = teamId;
    this.x = x;
    this.y = y;
    this.element = ENC.newAgent(id);
    this.targetTile = null;
  }
}

ENC.clickTile = function(tile){
  if(ENC.UI.selectState==="unselected"){
    if(tile.agentId != null){
      let agent = ENC.findAgentById(tile.agentId);
      if(agent.teamId != ENC.UI.turnTeam)
        return;
      if(agent.targetTile != null && agent.targetTile != tile){
        agent.targetTile.element.lastChild.remove();
        agent.targetTile = null;
      }
      ENC.UI.selectedTile=tile;
      agent.element.classList.add("blink");
      ENC.UI.selectState="selected";
    }
  }
  else if(ENC.UI.selectState==="selected"){
    if(Math.abs(ENC.UI.selectedTile.x-tile.x) <= 1 && Math.abs(ENC.UI.selectedTile.y-tile.y) <= 1){
      if(tile.agentId!=null)
        return
      let agent = ENC.findAgentById(ENC.UI.selectedTile.agentId);
      agent.element.classList.remove("blink");
      agent.targetTile=tile;
      ENC.UI.selectState="unselected";
      if(ENC.UI.selectedTile != tile){
        let copy = agent.element.cloneNode(true);
        copy.classList.add("blink");
        tile.element.appendChild(copy);
      }
      let action = ENC.newAction(ENC.UI.selectedTile,tile);
      ENC.upsertObjToArray(action,ENC.data.actions,"agentId");
    }
  }
}

ENC.makeTiles = function(json){
  let fieldDiv = ENC.fieldDiv;

  //make tiles
  let tiles_=[];
  let marginTop = Number(fieldDiv.style.marginTop.replace(/px/,""));
  let marginLeft = Number(fieldDiv.style.marginLeft.replace(/px/,""));
  for(let y=0,ylen=json.height;y<ylen;y++){
    let tiles=[]
    for(let x=0,xlen=json.width;x<xlen;x++){
      let tile = new ENC.Tile(json.points[y][x], json.tiled[y][x], x, y);
      tiles.push(tile);
    }
    tiles_.push(tiles);
  }
  return tiles_;
}

ENC.makeAgents = function(json){
  let agents = [];
  for(let i=0,ilen=json.teams.length;i<ilen;i++){
    for(let j=0,jlen=json.teams[i].agents.length;j<jlen;j++){
      let jsonAgent = json.teams[i].agents[j];
      let agent = new ENC.Agent(jsonAgent.agentID,json.teams[i].teamID,jsonAgent.x,jsonAgent.y);
      agents.push(agent);
    }
  }
  return agents;
}

ENC.insertAgents = function(agents,tiles){
  for(let i=0,len=agents.length;i<len;i++){
    let tile = tiles[agents[i].y][agents[i].x];
    tile.agentId = agents[i].id;
    tile.element.appendChild(agents[i].element);
    agents[i].tile = tile;
  }
}

ENC.findAgentById = function(id){
  return ENC.data.agents.find((one)=>one.id===id)
}

ENC.makeField = function(tiles){
  //init fieldDiv
  let fieldDiv = ENC.fieldDiv;
  fieldDiv.innerHTML="";
  fieldDiv.style.width=ENC.params.tileWidth*tiles[0].length+"px";
  fieldDiv.style.height=ENC.params.tileHeight*tiles.length+"px";
  for(let y=0,ylen=tiles.length; y<ylen; y++)
    for(let x=0,xlen=tiles.length; x<xlen; x++)
      fieldDiv.appendChild(tiles[y][x].element);
}

ENC.getNowTeam = function(){
  let sameAsFirstTeam = (ENC.UI.turn % 2);
  if(sameAsFirstTeam===1){
    return ENC.UI.firstTeam;
  }
  else
    return (ENC.UI.firstTeam==1)?2:1;
}

ENC.importJson = function(match){
  //let json = JSON.parse(match);
  let json = ENC.jsons.A1;
  ENC.data={};
  data = ENC.data;
  data.agents=[];
  data.actions=[];

  ENC.UI.selectState="unselected";
  ENC.UI.selectedTile=null;
  ENC.UI.turn=1;
  ENC.UI.turnTeam=ENC.getNowTeam();

  data.agents = ENC.makeAgents(json);
  data.tiles = ENC.makeTiles(json);
  
  ENC.makeField(data.tiles);
  ENC.insertAgents(data.agents,data.tiles)

  ENC.UI.total = ENC.calcPoints(ENC.data.tiles);
  ENC.updateInfo();

  ENC.getAgentById = function(id){
    return data.agents.find((one)=>id===one.agentID);
  }
}

ENC.newAction = function(agentTile,targetTile){
  let agentId = agentTile.agentId;
  let dx = parseInt(targetTile.x) - parseInt(agentTile.x);
  let dy = parseInt(targetTile.y) - parseInt(agentTile.y);
  return {
    agentId:agentId,
    dx:dx,
    dy:dy
  }
}

ENC.upsertObjToArray=function(obj,array,key){
  let updated = false;
  for(let i=0,len=array.length;i<len;i++){
    if(array[i][key] === obj[key]){
      array[i] = obj;
      updated = true;
      break;
    }
  }
  if(updated==false)
    array.push(obj);
}

ENC._applyAction=function(tiles, agents, actions=[]){
  for(let i=0,until=actions.length; i<until; i++){
    let action = actions[i];
    if(action.dx === 0 && action.dy === 0)
      continue;
    else{
      let agent = ENC.findAgentById(action.agentId);
      let agentTile =  ENC.data.tiles[agent.y][agent.x];
      let targetX = agent.x+action.dx;
      let targetY = agent.y+action.dy;
      if(targetX<0 || tiles[0].length<=targetX || targetY<0 || tiles.length<=targetY)
        continue; //out of field;
      let targetTile = tiles[targetY][targetX];
      if(targetTile.agentId === null){ //move
        if(targetTile.owner === 0 || targetTile.owner === agent.teamId){
          targetTile.owner = agent.teamId;
          targetTile.agent = agent.id;
          agentTile.agentId = null;
          agent.x=targetX;
          agent.y=targetY;
          targetTile.element.lastChild.remove();
          targetTile.element.appendChild(agent.element);
          targetTile.updateElement();
          agentTile.updateElement();
        }
        else{ //remove
          targetTile.owner = 0;
          targetTile.element.lastChild.remove();
          targetTile.updateElement();
        }
        continue;
      }
      //else (agent on targetTile)
    }
  }
}

ENC.makeActionsForUser = function(agents,teamId){
  let actions = [];
  for(let agent of agents)
    if(agent.teamId == teamId)
      actions.push({agentId:agent.id, dx:0, dy:0});
  return actions;
}

ENC.makeAgentsForUser = function(agents){
  let _agents = [];
  for(let agent of agents)
    _agents.push({id:agent.id, teamId:agent.teamId, x:agent.x, y:agent.y})
  return _agents;
}

ENC.makeTilesForUser = function(tiles){
  let _tiles=[];
  for(let y=0,height=tiles.length; y<height; y++){
    let row=[];
    for(let x=0,width=tiles[0].length; x<width; x++)
      row.push({point:tiles[y][x].point,owner:tiles[y][x].owner});
    _tiles.push(row);
  }
  return _tiles;
}

ENC.validateActions = function(actions,agents,teamId){
  let Alg = null;
  if(teamId===1 && ENC.redAutoRadio.checked === true && ENC.redAlgorithm!=null) //red
    Alg = (tiles_,agents_,actions_)=>{ENC.redAlgorithm(tiles_,agents_,actions_,ENC.redVariable)};
  else if(teamId===2 && ENC.blueAutoRadio.checked === true && ENC.blueAlgorithm!=null) //blue
    Alg = (tiles_,agents_,actions_)=>{ENC.blueAlgorithm(tiles_,agents_,actions_,ENC.blueVariable)};
  if(Alg!=null){
    let _tiles = ENC.makeTilesForUser(ENC.data.tiles);
    let _agents = ENC.makeAgentsForUser(agents);
    let _actions = ENC.makeActionsForUser(agents,teamId);
    Alg(_tiles,_agents,_actions);
    while(actions.length)
      actions.pop();
    for(let action of _actions)
      actions.push(action);
  }

  for(let i=0; i<actions.length;){
    if(ENC.findAgentById(actions[i].agentId).teamId != teamId)
      actions.splice(i,1);
    else if(actions[i].dx < -1 || 1 < actions[i].dx || actions[i].dy < -1 || 1 < actions[i].dy)
      actions.splice(i,1);
    else
      i++;
  }
  for(let i=0,until=agents.length; i<until; i++){
    let found = actions.find((one)=>one.agentId == agents[i].id)
    if(found === undefined){
      let action = {agentId:agents[i].id, dx:0, dy:0};
      actions.push(action);
    }
  }
}

ENC.updateInfo=function(){
  let str="";
  str += `ターン:${ENC.UI.turn}<br>`;
  str += `チーム:${ENC.UI.turnTeam==1?"赤":"青"}<br>`;
  str += `赤タイルポイント:${ENC.UI.total.tilePoints[0]}<br>`
  str += `赤エリアポイント:${ENC.UI.total.areaPoints[0]}<br>`
  str += `青タイルポイント:${ENC.UI.total.tilePoints[1]}<br>`
  str += `青エリアポイント:${ENC.UI.total.areaPoints[1]}<br>`
  ENC.infoDiv.innerHTML = str;
}

ENC.applyAction=function(){
  ENC.confirmButton.disabled = true;
  ENC.validateActions(ENC.data.actions,ENC.data.agents,ENC.UI.turnTeam);
  ENC._applyAction(ENC.data.tiles, ENC.data.agents, ENC.data.actions);
  ENC.makeField(ENC.data.tiles);
  ENC.insertAgents(ENC.data.agents,ENC.data.tiles);
  ENC.data.actions=[];

  ENC.UI.turn+=1;
  ENC.UI.turnTeam = ENC.getNowTeam();
  ENC.UI.total = ENC.calcPoints(ENC.data.tiles);
  ENC.updateInfo();
  if(ENC.UI.turn > ENC.UI.turnLimit){
    console.log("END GAME");
    return;
  }

  ENC.confirmButton.disabled = false;
}

ENC.confirmButton.addEventListener("click",()=>{ENC.applyAction()});

(async function(){
  try{
    //let res = await ENC.getRequest(`${window.location.href.replace(/index.html/,"")}fields/A-1.json`);
    //ENC.importJson(res.responseText);
    ENC.importJson("");
  }
  catch(err){
    throw err;
  }
}())